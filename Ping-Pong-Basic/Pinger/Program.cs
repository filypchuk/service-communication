﻿using RabbitMQ.Wrapper;
using System;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pinger started");
            RabbitWrapper rabbit = new RabbitWrapper("ping", "pong");
            rabbit.SendMessageToQueue();
            Console.ReadLine();
        }
    }
}
