﻿using RabbitMQ.Wrapper;
using System;
using System.Threading;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.Sleep(5000);
            Console.WriteLine("Ponger started");
            RabbitWrapper rabbit = new RabbitWrapper("pong", "ping");
            Console.ReadLine();
        }
    }
}
