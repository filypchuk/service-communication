﻿using System;
using RabbitMQ.Client;
using System.Text;
using RabbitMQ.Client.Events;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public class RabbitWrapper: IDisposable
    {
        private readonly EventingBasicConsumer _consumer;
        private readonly ConnectionFactory _factory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _own;
        private readonly string _sendTo;
        private readonly IBasicProperties _properties;
        public RabbitWrapper(string own, string sendTo)
        {
            _own = own;
            _sendTo = sendTo;
            _factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@localhost:5672")};
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();

            QueueExchangeDeclareAndBind(_own, _sendTo);            // declare Exchange and Queue and bind them (pinger exchange bind to ponger queue)
            QueueExchangeDeclareAndBind(_sendTo, _own);            // and vice versa

           

            _consumer = new EventingBasicConsumer(_channel);  // subscribe to the event (when we receive the message)
            _consumer.Received += ListenQueue;
            
            _channel.BasicConsume(queue: $"{_own}_queue",  // listening to the queue from which we will receive messages
                                 autoAck: false,
                                 consumer: _consumer);



            // additional settings

            _channel.BasicQos(prefetchSize: 0,        // BasicQos method with the prefetchCount = 1 setting.                                                   
                              prefetchCount: 1,       // This tells RabbitMQ not to give more than one message to a worker at a time.                                    
                              global: false);         // Or, in other words, don't dispatch a new message to a worker until it has processed and acknowledged the previous one.
                                                      // Instead, it will dispatch it to the next worker that is not still busy.
            
            _properties = _channel.CreateBasicProperties();   // This is if you want the messages to remain 
            _properties.Persistent = true;                    // even after a restart RabbitMQ server
        }
        private void QueueExchangeDeclareAndBind(string own, string send)
        {
            _channel.QueueDeclare(queue: $"{send}_queue",
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false);
            _channel.ExchangeDeclare($"{own}_exchange", ExchangeType.Fanout);
            _channel.QueueBind($"{send}_queue", $"{own}_exchange", "");
        }
        public void SendMessageToQueue()
        {
            Thread.Sleep(2500);
            Console.WriteLine(" [x] Sent {0} at {1}", _own, DateTime.Now);
            var body = Encoding.UTF8.GetBytes(_own);

            _channel.BasicPublish(exchange: $"{_own}_exchange",
                     routingKey: "",
                     basicProperties: _properties,
                     body: body);
        }
        private void ListenQueue(object sender, BasicDeliverEventArgs ea)
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine(" [x] Received {0} at {1}", message, DateTime.Now);
            _channel.BasicAck(ea.DeliveryTag, false);
            SendMessageToQueue();
        }
        public void Dispose()
        {
            _channel?.Close();
            _connection?.Close();
        }
        
    }
}
