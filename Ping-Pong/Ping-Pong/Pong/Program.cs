﻿using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.QueueServices;
using RabbitMQ.Client;
using System;
using RabbitMQ.Wrapper.Settings;

namespace Pong
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pong started");
            var uri = Settings.Uri;
            var connectionFactory = new ConnectionFactory() { Uri = uri };
            RabbitWrapper rabbit = new RabbitWrapper("pong", "ping",
                new MessageConsumerScopeFactory(connectionFactory),
                new MessageProduserScopeFactory(connectionFactory));

            Console.ReadLine();
        }
    }
}
