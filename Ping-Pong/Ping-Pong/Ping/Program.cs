﻿using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.QueueServices;
using RabbitMQ.Client;
using System;
using RabbitMQ.Wrapper.Settings;

namespace Ping
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ping started");
            var uri = Settings.Uri;
            var connectionFactory = new ConnectionFactory() { Uri = uri };
            RabbitWrapper rabbit = new RabbitWrapper("ping", "pong",
                new MessageConsumerScopeFactory(connectionFactory),
                new MessageProduserScopeFactory(connectionFactory));

            rabbit.SendMessageToQueue();
            Console.ReadLine();
        }
    }
}
