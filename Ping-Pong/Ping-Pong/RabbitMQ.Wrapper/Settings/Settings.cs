﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.Settings
{
    public static class Settings
    {
        public static Uri Uri { get; } = new Uri("amqp://guest:guest@localhost:5672");
    }
}
