﻿using RabbitMQ.Wrapper.Model;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProduserScopeFactory
    {
        IMessageProduserScope Open(MessageScopeSettings messageScopeSettings);
    
    }
}
