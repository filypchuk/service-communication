﻿using RabbitMQ.Wrapper.Model;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScopeFactory
    {
        IMessageConsumerScope Open(MessageScopeSettings messageScopeSettings);
        IMessageConsumerScope Connect(MessageScopeSettings messageScopeSettings);

    }
}
