﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScope:IDisposable
    {
        IMessageConsumer MessageConsumer { get;  }
        IMessageQueue MessageQueue { get; }

    }
}
