﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProduserScope : IDisposable
    {
        IMessageProduser MessageProduser { get; }
        IMessageQueue MessageQueue { get; }
    }
}
