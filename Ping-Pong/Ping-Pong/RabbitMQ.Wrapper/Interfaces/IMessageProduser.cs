﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProduser
    {
        void Send(string message, string type = null);
        void SendTyped(Type type, string message);
    }
}
