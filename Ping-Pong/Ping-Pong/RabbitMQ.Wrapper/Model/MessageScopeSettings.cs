﻿namespace RabbitMQ.Wrapper.Model
{
    public class MessageScopeSettings
    {
        public string ExchageName { get; set; }
        public string QueueName { get; set; }
        public string RoutingKey { get; set; }
        public string ExchageType { get; set; }
    }
}
