﻿using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Model
{
    public class MessageProduserSettings
    {
        public IModel Channel { get; set; }
        public PublicationAddress PublicationAddress { get; set; }
    }
}
