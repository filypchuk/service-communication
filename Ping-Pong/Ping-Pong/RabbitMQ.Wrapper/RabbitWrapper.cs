﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Model;
using System;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public class RabbitWrapper : IDisposable
    {
        private readonly IMessageConsumerScope _messageConsumeScope;
        private readonly IMessageProduserScope _messageProduserScope;
        private readonly string _listen;
        private readonly string _sendTo;
        public RabbitWrapper(string listen, string sendTo, 
                            IMessageConsumerScopeFactory messageConsumerScopeFactory,
                            IMessageProduserScopeFactory messageProduserScopeFactory)
        {
            _listen = listen;
            _sendTo = sendTo;

            _messageConsumeScope = messageConsumerScopeFactory.Connect(
                new MessageScopeSettings
                {
                    ExchageName = $"{_sendTo}_exchange",
                    ExchageType = ExchangeType.Direct,
                    QueueName = $"{_listen}_queue",
                    RoutingKey = ""
                });

            _messageConsumeScope.MessageConsumer.Received += MessageReceived;

            _messageProduserScope = messageProduserScopeFactory.Open(
               new MessageScopeSettings
               {
                   ExchageName = $"{_listen}_exchange",
                   ExchageType = ExchangeType.Direct,
                   QueueName = $"{_sendTo}_queue",
                   RoutingKey = ""
               });
        }
        private void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var processed = false;
            try
            {
                var body = args.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine(" [x] Received {0} at {1}", message, DateTime.Now);
                SendMessageToQueue();
                processed = true;
            }
            catch 
            {
                processed = false;
            }
            finally
            {
                _messageConsumeScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed);
            }
        }
        public void SendMessageToQueue()
        {
            Thread.Sleep(2500);
            Console.WriteLine(" [x] Sent {0} at {1}", _listen, DateTime.Now);
            _messageProduserScope.MessageProduser.Send(_listen);
        }
        public void Dispose()
        {
            _messageProduserScope?.Dispose();
            _messageConsumeScope?.Dispose();
        }
    }
}
