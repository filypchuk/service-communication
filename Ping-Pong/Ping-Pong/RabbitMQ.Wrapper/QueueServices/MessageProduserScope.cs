﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Model;
using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProduserScope: IMessageProduserScope
    {
        private readonly MessageScopeSettings _messageScopeSettings;
        private readonly Lazy<IMessageQueue> _messageQueueLazy;
        private readonly Lazy<IMessageProduser> _messageProduserLazy;
        private readonly IConnectionFactory _connectionFactory;
        public MessageProduserScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;

            _messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue);
            _messageProduserLazy = new Lazy<IMessageProduser>(CreateMessageProduser);

        }
        public IMessageQueue MessageQueue => _messageQueueLazy.Value;
        public IMessageProduser MessageProduser => _messageProduserLazy.Value;
        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSettings);
        }
        private IMessageProduser CreateMessageProduser()
        {
            return new MessageProduser(new MessageProduserSettings
            {
                Channel = MessageQueue.Channel,
                PublicationAddress = new PublicationAddress(
                    _messageScopeSettings.ExchageType,
                    _messageScopeSettings.ExchageName,
                    _messageScopeSettings.RoutingKey)

            });
        }
        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
