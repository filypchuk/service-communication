﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Model;
using System;
using System.Text;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProduser: IMessageProduser
    {
        private readonly MessageProduserSettings _messageProduserSettings;
        private readonly IBasicProperties _properties;

        public MessageProduser(MessageProduserSettings messageProduserSettings)
        {
            _messageProduserSettings = messageProduserSettings;
            _properties = _messageProduserSettings.Channel.CreateBasicProperties();
            _properties.Persistent = true;
        }
        public void Send(string message, string type = null)
        {
            if(!string.IsNullOrEmpty(type))
            {
                _properties.Type = type;
            }
            var body = Encoding.UTF8.GetBytes(message);
            _messageProduserSettings.Channel.BasicPublish(_messageProduserSettings.PublicationAddress, _properties, body);
        }

        public void SendTyped(Type type, string message)
        {
            Send(message, type.AssemblyQualifiedName);
        }
    }
}
