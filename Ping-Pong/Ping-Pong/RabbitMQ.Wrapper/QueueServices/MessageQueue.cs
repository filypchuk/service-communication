﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Model;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageQueue: IMessageQueue
    {
        private readonly IConnection _connection;
        public IModel Channel { get; protected set; }
        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }
        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings) : this(connectionFactory)
        {
            DeclareExchange(messageScopeSettings.ExchageName, messageScopeSettings.ExchageType);

            if(messageScopeSettings.QueueName != null)
            {
                BindQueue(messageScopeSettings.ExchageName, messageScopeSettings.RoutingKey, messageScopeSettings.QueueName);
            }
        }

        public void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }
        public void BindQueue(string exchangeName, string routingKey, string queueName)
        {
            Channel.QueueDeclare(queueName, true, false, false);
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }
        public void Dispose()
        {
            Channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
