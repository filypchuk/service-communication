﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Model;
using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumerScope: IMessageConsumerScope
    {
        private readonly MessageScopeSettings _messageScopeSettings;
        private readonly Lazy<IMessageQueue> _messageQueueLazy;
        private readonly Lazy<IMessageConsumer> _messageConsumerLazy;
        private readonly IConnectionFactory _connectionFactory;
        public MessageConsumerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;

            _messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue);
            _messageConsumerLazy = new  Lazy<IMessageConsumer>(CreateMessageConsumer);

        }
        public IMessageQueue MessageQueue => _messageQueueLazy.Value;
        public IMessageConsumer MessageConsumer => _messageConsumerLazy.Value;

        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSettings);
        }
        private IMessageConsumer CreateMessageConsumer()
        {
            return new MessageConsumer(new MessageConsumerSettings
            {   
                Channel = MessageQueue.Channel,
                QueueName = _messageScopeSettings.QueueName 
            });
        }
        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
             

    }
}
