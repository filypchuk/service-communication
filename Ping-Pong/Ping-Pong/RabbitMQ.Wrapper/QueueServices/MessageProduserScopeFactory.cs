﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Model;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProduserScopeFactory: IMessageProduserScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;
        public MessageProduserScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public IMessageProduserScope Open(MessageScopeSettings messageScopeSettings)
        {
            return new MessageProduserScope(_connectionFactory, messageScopeSettings);
        }
    }


}
